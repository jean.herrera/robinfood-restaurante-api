package com.robinfood.restauranteapi.controller;

import com.robinfood.restauranteapi.dto.EncuestaDto;
import com.robinfood.restauranteapi.mapper.EncuestaMapper;
import com.robinfood.restauranteapi.model.Encuesta;
import com.robinfood.restauranteapi.repository.EncuestaRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class EncuestaControllerTest{

    @InjectMocks
    private EncuestaController encuestaController;

    @Mock
    private EncuestaMapper encuestaMapper;

    @Mock
    private EncuestaRepository encuestaRepository;

    @Test
    public void createEncuesta() {
        Encuesta encuesta = new Encuesta();
        encuesta.setId(1);
        encuesta.setNombreEncuesta("Encuesta prueba");
        encuesta.setDescripcionEncuesta("Prueba encuesta RobbinFood");

        lenient().when(encuestaRepository.save(ArgumentMatchers.any(Encuesta.class))).thenReturn(encuesta);

        var encuestaCreada = encuestaController.createEncuesta(encuestaMapper.modelToDto(encuesta));
        assertEquals(201, encuestaCreada.getStatusCode().value());
    }

    @Test
    public void listEncuestas(){
        var encuestasResponse = new ArrayList<Encuesta>();

        Encuesta encuesta1 = new Encuesta();
        encuesta1.setId(1);
        encuesta1.setNombreEncuesta("Encuesta prueba 1");
        encuesta1.setDescripcionEncuesta("Prueba encuesta RobbinFood - Cartagena");

        Encuesta encuesta2 = new Encuesta();
        encuesta2.setId(2);
        encuesta2.setNombreEncuesta("Encuesta prueba 2");
        encuesta2.setDescripcionEncuesta("Prueba encuesta RobbinFood - Medellín");

        encuestasResponse.add(encuesta1);
        encuestasResponse.add(encuesta2);

        lenient().when(encuestaRepository.findAll()).thenReturn(encuestasResponse);

        var listaEncuestas = encuestaController.getAllEncuestas();
        assertEquals(200, listaEncuestas.getStatusCode().value());

    }

    /*@Test
    void findById() {
        Encuesta encuesta = new Encuesta();
        encuesta.setId(1);
        encuesta.setNombreEncuesta("Encuesta prueba 1");
        encuesta.setDescripcionEncuesta("Prueba encuesta RobbinFood - Cartagena");

        lenient().when(encuestaRepository.findById(1)).thenReturn(encuestaMapper.modelToDto(encuesta));

        //var response = encuestaController.buscar(1);

        assertEquals(200, response.getStatusCode().value());

    }*/
}
