package com.robinfood.restauranteapi.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "Encuesta")
public class Encuesta {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "nombreEncuesta")
    private String nombreEncuesta;

    @Column(name = "descripcionEncuesta")
    private String descripcionEncuesta;

    @OneToMany(targetEntity = Pregunta.class, cascade={CascadeType.ALL})
    @JoinColumn(name = "idEncuesta", referencedColumnName = "id")
    private List<Pregunta> preguntas;

    @OneToMany(targetEntity = Respuesta.class, cascade={CascadeType.ALL})
    @JoinColumn(name = "idEncuesta", referencedColumnName = "id")
    private List<Respuesta> respuestas;

}
