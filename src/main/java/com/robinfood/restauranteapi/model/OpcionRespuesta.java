package com.robinfood.restauranteapi.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "Opcion_Respuesta")
public class OpcionRespuesta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "texto")
    private String texto;

    @JsonBackReference(value = "idPregunta")
    @ManyToOne
    @JoinColumn(name = "idPregunta")
    private Pregunta pregunta;

    @OneToMany(mappedBy = "opcionRespuesta")
    private List<Respuesta> respuesta;

}
