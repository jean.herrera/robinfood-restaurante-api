package com.robinfood.restauranteapi.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "Pregunta")
public class Pregunta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "pregunta")
    private String pregunta;

    @OneToOne
    @JoinColumn(name = "idTipoPregunta")
    private TipoPregunta tipoPregunta;

    @OneToMany(targetEntity = OpcionRespuesta.class, cascade={CascadeType.ALL})
    @JoinColumn(name = "idPregunta", referencedColumnName = "id")
    private List<OpcionRespuesta> opcionRespuestas;

    @JsonBackReference(value = "idEncuesta")
    @ManyToOne
    @JoinColumn(name = "idEncuesta")
    private Encuesta encuesta;

    @OneToMany(mappedBy="pregunta", cascade={CascadeType.ALL})
    private List<Respuesta> respuesta;
}
