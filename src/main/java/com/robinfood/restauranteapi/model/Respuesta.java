package com.robinfood.restauranteapi.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "Respuesta")
public class Respuesta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "respuestaAbierta")
    private String respuestaAbierta;

    @JsonBackReference(value = "repuesta-idencuesta")
    @ManyToOne
    @JoinColumn(name = "idEncuesta")
    private Encuesta encuesta;

    @JsonBackReference(value = "repuesta-opcion-respuesta")
    @ManyToOne
    @JoinColumn(name = "idOpcionRespuesta")
    private OpcionRespuesta opcionRespuesta;

    @JsonBackReference(value = "repuesta-pregunta")
    @ManyToOne
    @JoinColumn(name = "idPregunta")
    private Pregunta pregunta;

    //@JsonBackReference(value = "respuesta-cliente")
    @ManyToOne(targetEntity = Cliente.class, cascade={CascadeType.ALL})
    @JoinColumn(name = "idCliente", referencedColumnName = "id")
    private Cliente cliente;
}
