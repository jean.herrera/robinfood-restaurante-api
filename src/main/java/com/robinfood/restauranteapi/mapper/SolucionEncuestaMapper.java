package com.robinfood.restauranteapi.mapper;

import com.robinfood.restauranteapi.dto.SolucionEncuestaDto;
import com.robinfood.restauranteapi.model.Respuesta;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface SolucionEncuestaMapper {

    //Uso del patrón Factory
    SolucionEncuestaMapper INSTANCE = Mappers.getMapper(SolucionEncuestaMapper.class);

    Respuesta dtoToModel(SolucionEncuestaDto solucionEncuestaDto);
}
