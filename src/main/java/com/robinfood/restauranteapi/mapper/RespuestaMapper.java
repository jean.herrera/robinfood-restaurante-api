package com.robinfood.restauranteapi.mapper;

import com.robinfood.restauranteapi.dto.RespuestaDto;
import com.robinfood.restauranteapi.model.Respuesta;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RespuestaMapper {

    //Uso del patrón Factory
    RespuestaMapper INSTANCE = Mappers.getMapper(RespuestaMapper.class);

    List<RespuestaDto> modelsToDtos(List<Respuesta> respuestas);
    RespuestaDto modelToDto(Respuesta respuesta);
    Respuesta dtoToModel(RespuestaDto respuestaDto);
}
