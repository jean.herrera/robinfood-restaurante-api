package com.robinfood.restauranteapi.mapper;

import com.robinfood.restauranteapi.dto.EncuestaDto;
import com.robinfood.restauranteapi.dto.PreguntaDto;
import com.robinfood.restauranteapi.model.Encuesta;
import com.robinfood.restauranteapi.model.Pregunta;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PreguntaMapper {

    //Uso del patrón Factory
    PreguntaMapper INSTANCE = Mappers.getMapper(PreguntaMapper.class);


    List<PreguntaDto> modelsToDtos(List<Pregunta> preguntas);

    PreguntaDto modelToDto(Pregunta pregunta);
    Pregunta dtoToModel(PreguntaDto preguntaDto);
}
