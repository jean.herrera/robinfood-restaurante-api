package com.robinfood.restauranteapi.mapper;

import com.robinfood.restauranteapi.dto.EncuestaDto;
import com.robinfood.restauranteapi.model.Encuesta;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface EncuestaMapper {

    //Uso del patrón Factory
    EncuestaMapper INSTANCE = Mappers.getMapper(EncuestaMapper.class);

    List<EncuestaDto> modelsToDtos(List<Encuesta> encuestas);
    EncuestaDto modelToDto(Encuesta encuesta);
    Encuesta dtoToModel(EncuestaDto encuestaDto);
}
