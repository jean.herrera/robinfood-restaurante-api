package com.robinfood.restauranteapi.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RespuestaDto {

    private List<OpcionRespuestaDto> opcionRespuestas;
    private String respuestaAbierta;
}
