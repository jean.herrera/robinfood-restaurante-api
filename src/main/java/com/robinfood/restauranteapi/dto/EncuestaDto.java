package com.robinfood.restauranteapi.dto;

import com.robinfood.restauranteapi.model.Pregunta;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class EncuestaDto {

    private int id;
    private String nombreEncuesta;
    private String descripcionEncuesta;
    private List<Pregunta> preguntas;
}
