package com.robinfood.restauranteapi.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PreguntaDto {

    private int id;
    private RespuestaDto respuestas;

}
