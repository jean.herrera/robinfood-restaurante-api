package com.robinfood.restauranteapi.dto;

import com.robinfood.restauranteapi.model.Pregunta;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class SolucionEncuestaDto {

    private int id;
    private int idEncuesta;
    private int idCliente;
    private List<Pregunta> preguntas;
}
