package com.robinfood.restauranteapi.controller;

import com.robinfood.restauranteapi.dto.EncuestaDto;
import com.robinfood.restauranteapi.mapper.EncuestaMapper;
import com.robinfood.restauranteapi.model.Encuesta;
import com.robinfood.restauranteapi.repository.EncuestaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/encuestas")

public class EncuestaController {

    @Autowired
    private EncuestaMapper encuestaMapper;

    @Autowired
    private EncuestaRepository encuestaRepository;

    @GetMapping
    public ResponseEntity<List<EncuestaDto>> getAllEncuestas(){
        return new ResponseEntity<>(encuestaMapper.modelsToDtos(encuestaRepository.findAll()), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Encuesta> createEncuesta(@RequestBody EncuestaDto encuestaDto){
        return new ResponseEntity<>(encuestaRepository.save(
                encuestaMapper.dtoToModel(encuestaDto)),HttpStatus.CREATED);

    }

    @GetMapping("{id}")
    public ResponseEntity<EncuestaDto> getEncuestaByID(@PathVariable(value = "id") Integer id){
        return new ResponseEntity<>(encuestaMapper.modelToDto(encuestaRepository.findById(id).get()),HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<HttpStatus> deleteEncuesta(@PathVariable(value = "id") Integer id){
        EncuestaDto encuestaDto = encuestaMapper.modelToDto(encuestaRepository.findById(id).get());
        encuestaRepository.deleteById(encuestaDto.getId());
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
