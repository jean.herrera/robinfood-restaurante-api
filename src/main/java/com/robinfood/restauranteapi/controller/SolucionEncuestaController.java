package com.robinfood.restauranteapi.controller;


import com.robinfood.restauranteapi.dto.EncuestaDto;
import com.robinfood.restauranteapi.dto.RespuestaDto;
import com.robinfood.restauranteapi.dto.SolucionEncuestaDto;
import com.robinfood.restauranteapi.mapper.RespuestaMapper;
import com.robinfood.restauranteapi.mapper.SolucionEncuestaMapper;
import com.robinfood.restauranteapi.model.Encuesta;
import com.robinfood.restauranteapi.model.Respuesta;
import com.robinfood.restauranteapi.repository.RespuestaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/solucionEncuesta")
public class SolucionEncuestaController {

    @Autowired
    private RespuestaMapper respuestaMapper;

    @Autowired
    private SolucionEncuestaMapper solucionEncuestaMapper;

    @Autowired
    private RespuestaRepository respuestaRepository;

    @PostMapping
    public ResponseEntity<Respuesta> saveAnswerEncuesta(@RequestBody SolucionEncuestaDto solucionEncuestaDto){
        return new ResponseEntity<>(respuestaRepository.save(
                solucionEncuestaMapper.dtoToModel(solucionEncuestaDto)),HttpStatus.CREATED);

    }
}
