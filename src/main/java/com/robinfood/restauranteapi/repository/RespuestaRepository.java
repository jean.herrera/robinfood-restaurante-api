package com.robinfood.restauranteapi.repository;

import com.robinfood.restauranteapi.model.Respuesta;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RespuestaRepository extends JpaRepository<Respuesta, Integer> {

}
