package com.robinfood.restauranteapi.repository;

import com.robinfood.restauranteapi.model.Encuesta;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EncuestaRepository extends JpaRepository<Encuesta, Integer> {


}
